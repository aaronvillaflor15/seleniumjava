package seleniumframework.pageobjects.locators;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class ProductPageLocators {

	  public static class TextLabel
      {
	
		public static By swagLabs = By.className("app_logo");
		public static By products = By.className("title");
		public static By prices = By.className("inventory_item_price");
		public static By summaryInfo = By.className("summary_info_label");
		public static By summaryInfoValue = By.className("summary_value_label");
		public static By paymentInfo = By.cssSelector(".summary_info_label:nth-child(1)");
		public static By shippingInfo = By.cssSelector(".summary_info_label:nth-child(3)");
		public static By priceTotal = By.cssSelector(".summary_info_label:nth-child(5)");
		public static By total = By.cssSelector(".summary_info_label:nth-child(8)");
		public static By paymentInfoValue = By.cssSelector(".summary_value_label:nth-child(2)");
		public static By shippingInfoValue = By.cssSelector(".summary_value_label:nth-child(4)");
		public static By itemTotalValue = By.cssSelector(".summary_subtotal_label");
		public static By tax = By.cssSelector(".summary_tax_label");
		public static By thankYouHeader = By.cssSelector(".complete-header");
      }
	  public static class Button
      {
	
		public static By boltTshirtAddToCart = By.id("add-to-cart-sauce-labs-bolt-t-shirt");
		public static By allTheThingsAddToCart = By.id("add-to-cart-test.allthethings()-t-shirt-(red)");
		public static By shoppingCart = By.cssSelector(".shopping_cart_link");
		public static By checkOut = By.id("checkout");
		public static By cont = By.id("continue");
		public static By finish = By.id("finish");
	
      }
	  
	  public static class Container
      {
          public static By dropdown = By.className("product_sort_container");
          
      }
	  public static class TextField
      {
	
		public static By firstName = By.id("first-name");
		public static By lastName = By.id("last-name");
		public static By postalCode = By.id("postal-code");
		  
      }
}

