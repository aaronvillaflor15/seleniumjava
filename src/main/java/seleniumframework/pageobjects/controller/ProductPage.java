package seleniumframework.pageobjects.controller;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import seleniumframework.pageobjects.locators.ProductPageLocators;

public class ProductPage {

	WebDriver driver;

	public ProductPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		this.driver = driver;
	}

	public void waitForElementToAppear(By findBy) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		wait.until(ExpectedConditions.visibilityOfElementLocated(findBy));
	}

	public String getText(By findBy) {
		String text = driver.findElement(findBy).getText();
		return text;
	}

	public void sortHighToLow() {
		// locate element of prices
		List<WebElement> beforeFilterPrices = driver.findElements(ProductPageLocators.TextLabel.prices);
		// create new array list
		List<Double> beforeFilterPriceList = new ArrayList<>();

		for (WebElement price : beforeFilterPrices) {
			beforeFilterPriceList.add(Double.valueOf(price.getText().replace("$", "")));
		}

		selectFromDropDown(ProductPageLocators.Container.dropdown, "Price (high to low)");

//		Select dropDown = new
//		Select(driver.findElement(ProductPageLocators.Container.dropdown));
//		dropDown.selectByVisibleText("Price (high to low)");

		List<WebElement> afterFilterPrices = driver.findElements(ProductPageLocators.TextLabel.prices);

		List<Double> afterFilterPriceList = new ArrayList<>();
		for (WebElement price : afterFilterPrices) {
			afterFilterPriceList.add(Double.valueOf(price.getText().replace("$", "")));
		}

		Collections.sort(beforeFilterPriceList);
		Collections.reverse(beforeFilterPriceList);

		Assert.assertEquals(beforeFilterPriceList, afterFilterPriceList);
		System.out.println(beforeFilterPriceList);
		System.out.println(afterFilterPriceList);


	}
	public void summaryLogs() {

		String payment = getText(ProductPageLocators.TextLabel.paymentInfo);
		String paymentValue = getText(ProductPageLocators.TextLabel.paymentInfoValue);
		System.out.println(payment + ": " + paymentValue);

		
		String shipping = getText(ProductPageLocators.TextLabel.shippingInfo);
		String shippingValue = getText(ProductPageLocators.TextLabel.shippingInfoValue);
		System.out.println(shipping  + ": " + shippingValue);
		
		String priceTotal = getText(ProductPageLocators.TextLabel.priceTotal);
		String itemTotalValue = getText(ProductPageLocators.TextLabel.itemTotalValue);
		String tax = getText(ProductPageLocators.TextLabel.tax);
		System.out.println(priceTotal);
		System.out.println(itemTotalValue);
		System.out.println(tax);
		
		String total = getText(ProductPageLocators.TextLabel.total);
		System.out.println(total);

	}

	public void selectFromDropDown(By findBy, String text) {
		Select dropDown = new Select(driver.findElement(findBy));
		dropDown.selectByVisibleText(text);

	}

	public void checkOut() {
		//waitForElementToAppear(ProductPageLocators.Button.shoppingCart);
		driver.findElement(ProductPageLocators.Button.shoppingCart).click();
		driver.findElement(ProductPageLocators.Button.checkOut).click();

	}
	public void fillInformation() {
		//waitForElementToAppear(ProductPageLocators.Button.shoppingCart);
		driver.findElement(ProductPageLocators.TextField.firstName).sendKeys("testfirstname");
		driver.findElement(ProductPageLocators.TextField.lastName).sendKeys("testsurname");
		driver.findElement(ProductPageLocators.TextField.postalCode).sendKeys("testcode");
		driver.findElement(ProductPageLocators.Button.cont).click();

	}

	public void purchaseProduct() {

		driver.findElement(ProductPageLocators.Button.allTheThingsAddToCart).click();
		driver.findElement(ProductPageLocators.Button.boltTshirtAddToCart).click();

	}
	public void clickFinish() {

		driver.findElement(ProductPageLocators.Button.finish).click();
		Assert.assertEquals("Thank you for your order!", getText(ProductPageLocators.TextLabel.thankYouHeader));
		

	}

}
