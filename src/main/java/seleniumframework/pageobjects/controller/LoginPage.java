package seleniumframework.pageobjects.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import seleniumframework.pageobjects.locators.LoginPageLocators;
import seleniumframework.pageobjects.locators.ProductPageLocators;

public class LoginPage {

	WebDriver driver;
	ProductPage productPage;
	Properties prop = new Properties();



	String browserName = System.getProperty("browser")!=null ? System.getProperty("browser") : prop.getProperty("browser");

	//prop.getProperty("browser");
	public LoginPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		// init pages
		this.driver = driver;
		productPage = new ProductPage(driver);
	}


	public void loginValidUser(String usr, String pss) throws IOException {
		

		
		driver.findElement(LoginPageLocators.TextField.username).sendKeys(credsUsername(usr));
		driver.findElement(LoginPageLocators.TextField.password).sendKeys(credsPassword(pss));
		driver.findElement(LoginPageLocators.Button.login).click();
		productPage.waitForElementToAppear(ProductPageLocators.TextLabel.swagLabs);
		productPage.waitForElementToAppear(ProductPageLocators.TextLabel.products);
		Assert.assertEquals("Swag Labs", productPage.getText(ProductPageLocators.TextLabel.swagLabs));
		Assert.assertEquals("Products", productPage.getText(ProductPageLocators.TextLabel.products));
	}
	
	public void loginLockedUser(String usr, String pss) throws IOException {
		
	
			
			driver.findElement(LoginPageLocators.TextField.username).sendKeys(credsUsername(usr));
			driver.findElement(LoginPageLocators.TextField.password).sendKeys(credsPassword(pss));
			driver.findElement(LoginPageLocators.Button.login).click();
			productPage.waitForElementToAppear(LoginPageLocators.TextField.errMessage);
			Assert.assertEquals("Epic sadface: Sorry, this user has been locked out.", productPage.getText(LoginPageLocators.TextField.errMessage));
			
		}
	
	public String credsUsername(String username) throws IOException {
		
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
				+ "//src//test//resources//configfiles//config.properties");
		prop.load(fis);
		String user = System.getProperty(username)!=null ? System.getProperty(username) : prop.getProperty(username);
		return user;

}
	public String credsPassword(String password) throws IOException {
		
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")
				+ "//src//test//resources//configfiles//config.properties");
		prop.load(fis);
		String pass = System.getProperty(password)!=null ? System.getProperty(password) : prop.getProperty(password);
		return pass;

}
}
