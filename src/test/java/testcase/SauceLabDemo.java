package testcase;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import basetest.BaseTest;
import seleniumframework.pageobjects.controller.LoginPage;
import seleniumframework.pageobjects.controller.ProductPage;

public class SauceLabDemo extends BaseTest {

	LoginPage loginPage;
	ProductPage productPage;

	@BeforeClass
	public void initPages() {
		loginPage = new LoginPage(driver);
		productPage = new ProductPage(driver);
	}

	@Test
	public void sauceDemoTestCase() throws IOException {

		loginPage.loginValidUser("usernameStandard", "password");
		productPage.sortHighToLow();
		productPage.purchaseProduct();
		productPage.checkOut();
		productPage.fillInformation();
		productPage.summaryLogs();
		productPage.clickFinish();

	}

	@Test
	public void validateLockedUser() throws IOException {

		loginPage.loginLockedUser("usernameLocked", "password");

	}

}
